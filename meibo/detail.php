
<!DOCTYPE html>
<?php
  include("./include/statics.php");
  $DB_DSN = "mysql:host=localhost; dbname=meibo; charset=utf8";
  $DB_USER = "meibo";
  $DB_PW = "b9SvYEEgn92KEdUK";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $imp_ID = 0; //編集の初期化
  $imp_ID = $_GET['member_ID']; //持ってきた社員IDを代入

  //ページ進入時DBに投げるSQL文
  $query_str = "SELECT member.member_ID, member.name, member.pref, member.seibetu,
                       member.age, grade_master.grade_name, section1_master.section_name
                FROM `member`
                LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                WHERE `member_ID` = $imp_ID
                ";
  //aqlとして送る文の中身を表示
  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>
<!---DBから帰って来たものの確認
<pre align="left">
  <?php
    var_dump($result);
  ?>
</pre>
--->
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員情報</title>
    <h1>社員名簿システム</h1>
      <!---ページ遷移ボタン--->
      <div align="right">
        <a href="index.php">トップ画面</a><a> </a>
        <a href="entry01.php">新規社員登録へ</a>
      </div>
  </head>
  <hr>

  <!---社員情報表示--->
  <?php
  foreach ($result as $each){
     $each['name'] . $each['pref'] . $each['seibetu'] . $each['age'] . $each['section_name'] . $each['grade_name'];
  }
  ?>
  <div align="center">
    <br>
    <table border="1">
      <tbody>
        <tr>
          <th>社員ID</th>
          <td><?php echo $imp_ID ?></td>
        </tr>
        <tr>
          <th>名前</th>
          <td><?php echo $each['name']; ?></td>
        </tr>
        <tr>
          <th>出身地</th>
          <td><?php echo $pref[$result[0]['pref']]; ?></td>
        </tr>
        <tr>
          <th>性別</th>
          <td><?php echo $each['seibetu']; ?></td>
        </tr>
        <tr>
          <th>年齢</th>
          <td><?php echo $each['age']; ?></td>
        </tr>
        <tr>
          <th>所属部署</th>
          <td><?php echo $result[0]['section_name']; ?></td>
        </tr>
        <tr>
          <th>役職</th>
          <td><?php echo $result[0]['grade_name']; ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <form class="" action="entry_update01.php" method="POST" align="right">
    <input type="submit" name="edit" value="編集">
  </form>
  <form action="detail02.php" method="POST" align="right">
    <input type="submit" name="delete" value="削除">
    <input type="hidden" name="delete_member_ID"
           value="<?php echo $result[0]['member_ID']; ?>">
  </form>

</html>
