
<!DOCTYPE html>
<?php
//変数の初期化
$imp_edit = "";
if(isset($_POST['edit_data']) && $_POST['edit_data'] != ""){
    $imp_edit = $_POST['edit_data'];
}
echo $imp_edit;
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員情報</title>
    <h1>社員名簿システム</h1>
      <!---ページ遷移ボタン--->
      <div align="right">
        <a href="index.php">トップ画面</a><a> </a>
        <a href="entry01.php">新規社員登録へ</a>
      </div>
  </head>
  <hr>

  <!---編集する社員情報--->
  <div align="center">
    <form action="entry02.php" method="GET"><!---登録後sqlを投げるダミーページに飛ぶ--->
      <!---社員情報表示テーブル--->
      <table border="1">
        <tr>
          <th>名前</th>
          <td><input type="text" name="add_name" value="<?php echo $imp_edit;  ?>"></td>
        </tr>
        <tr>
          <th>出身</th>
          <td><select name="add_pref">
              <option value="">都道府県</option>
              <option value="1">北海道</option>
              <option value="2">青森県</option>
              <option value="3">岩手県</option>
              <option value="4">宮城県</option>
              <option value="5">秋田県</option>
              <option value="6">山形県</option>
              <option value="7">福島県</option>
              <option value="8">茨城県</option>
              <option value="9">栃木県</option>
              <option value="10">群馬県</option>
              <option value="11">埼玉県</option>
              <option value="12">千葉県</option>
              <option value="13">東京都</option>
              <option value="14">神奈川県</option>
              <option value="15">新潟県</option>
              <option value="16">富山県</option>
              <option value="17">石川県</option>
              <option value="18">福井県</option>
              <option value="19">山梨県</option>
              <option value="20">長野県</option>
              <option value="21">岐阜県</option>
              <option value="22">静岡県</option>
              <option value="23">愛知県</option>
              <option value="24">三重県</option>
              <option value="25">滋賀県</option>
              <option value="26">京都府</option>
              <option value="27">大阪府</option>
              <option value="28">兵庫県</option>
              <option value="29">奈良県</option>
              <option value="30">和歌山県</option>
              <option value="31">鳥取県</option>
              <option value="32">島根県</option>
              <option value="33">岡山県</option>
              <option value="34">広島県</option>
              <option value="35">山口県</option>
              <option value="36">徳島県</option>
              <option value="37">香川県</option>
              <option value="38">愛媛県</option>
              <option value="39">高知県</option>
              <option value="40">福岡県</option>
              <option value="41">佐賀県</option>
              <option value="42">長崎県</option>
              <option value="43">熊本県</option>
              <option value="44">大分県</option>
              <option value="45">宮崎県</option>
              <option value="46">鹿児島県</option>
              <option value="47">沖縄県</option>
          </select></td>
        </tr>
        <tr>
          <th>性別</th>
          <td>
            <input type="radio" name="add_sex" value="男" checked>男
            <input type="radio" name="add_sex" value="女">女
          </td>
        </tr>
        <tr>
          <th>年齢</th>
          <td><input type="number" name="add_age" placeholder="年齢を入力"></td>
        </tr>
        <tr>
          <th>所属部署</th>
          <td>
            <input type="radio" name="add_section_ID" value="1" checked>第一事業部
            <input type="radio" name="add_section_ID" value="2">第二事業部
            <input type="radio" name="add_section_ID" value="3">営業
            <input type="radio" name="add_section_ID" value="4">総務
            <input type="radio" name="add_section_ID" value="5">人事
          </td>
        </tr>
        <tr>
          <th>役職</th>
          <td>
            <input type="radio" name="add_grade_ID" value="1" checked>事業部長
            <input type="radio" name="add_grade_ID" value="2">部長
            <input type="radio" name="add_grade_ID" value="3">チームリーダー
            <input type="radio" name="add_grade_ID" value="4">リーダー
            <input type="radio" name="add_grade_ID" value="5">メンバー
          </td>
        </tr>
      </table>
        <input type="submit" name="add_button" value="登録" align="right">
        <input type="reset" name="reset" value="リセットする" align="right">
    </form>
  </div>

</html>
