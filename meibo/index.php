
<!DOCTYPE html>
<?php

  $DB_DSN = "mysql:host=localhost; dbname=meibo; charset=utf8";
  $DB_USER = "meibo";
  $DB_PW = "b9SvYEEgn92KEdUK";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
/*
  //変数の初期化
  $$oldsearch_id = "";
  $oldsearch_name = "";
  $$oldsearch_sex = "";
  $$oldsearch_section = "";
  $oldsearch_grade = "";


  //初期化のチェック
  if (isset($_GET['member_ID']) && $_GET['member_ID'] != "") {
    $$oldsearch_id = $_GET['member_ID'];
  }
  if (isset($_GET['name']) && $_GET['name'] != "") {
    $oldsearch_name = $_GET['name'];
  }
  if (isset($_GET['seibetu']) && $_GET['seibetu'] != "") {
    $$oldsearch_sex = $_GET['seibetu'];
  }
  if (isset($_GET['search_section']) && $_GET['search_section'] != "") {
    $$oldsearch_section = $_GET['search_section'];
  }
  if (isset($_GET['grade_ID']) && $_GET['grade_ID'] != "") {
    $oldsearch_grade = $_GET['grade_ID'];
  }
*/

  $query_str = "SELECT member.member_ID, member.name, grade_master.grade_name,
                       section1_master.section_name
                FROM `member`
                LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                WHERE 1=1
                ";
    //ID
    if (isset($_GET['member_ID']) && $_GET['member_ID'] != "") {
      $query_str .= " AND member_ID = '" . $_GET['member_ID'] . "'";
    }
    //名前
    if (isset($_GET['search_name']) && $_GET['search_name'] != "") {
      $query_str .= " AND name LIKE '%" . $_GET['search_name'] . "%'";
    }
    //性別
    if (isset($_GET['search_sex']) && $_GET['search_sex'] != "") {
      $query_str .= " AND seibetu LIKE '%" . $_GET['search_sex'] . "%'";
    }
    //部署
    if (isset($_GET['search_section']) && $_GET['search_section'] != "") {
      $query_str .= " AND section_name LIKE '%" . $_GET['search_section'] . "%'";
    }
    //役職
    if (isset($_GET['search_grade']) && $_GET['search_grade'] != "") {
      $query_str .= " AND grade_name LIKE '%" . $_GET['search_grade'] . "%'";
    }
  //aqlとして送る文の中身を表示
  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>飯処ウェブレッジ水道橋店</title>
    <h1>社員名簿システム</h1>
      <!---ページ遷移ボタン--->
      <div align="right">
        <a href="index.php">トップ画面</a><a> </a>
        <a href="entry01.php">新規社員登録へ</a>
      </div>

  </head>

  <!---検索フォーム--->
  <hr>
  <div align="center">
    <form action="index.php" method="GET">
      <label>名前：</label>
        <input type="text" name="search_name" value="" size="30"><br>
      <label>性別：</label>
        <select name="search_sex">
          <option value="">すべて</option>
          <option value="2">男</option>
          <option value="1">女</option>
        </select>
      <label>部署：</label>
        <select name="search_section">
          <option value="">すべて</option>
          <option value="第一事業部">第一事業部</option>
          <option value="第二事業部">第二事業部</option>
          <option value="営業">営業</option>
          <option value="総務">総務</option>
          <option value="人事">人事</option>
        </select>
      <label>役職：</label>
        <select name="search_grade">
          <option value="">すべて</option>
          <option value="事業部長">事業部長</option>
          <option value="部長">部長</option>
          <option value="チームリーダー">チームリーダー</option>
          <option value="リーダー">リーダー</option>
          <option value="メンバー">メンバー</option>
        </select>
      <br>
      <input type="submit" name="search" value="検索">
      <input type="reset" name="reset" value="リセットする">
    </form>
  </div>
  <hr>

<!---検索結表示テーブル--->
  <body>
    <div align="center">
      <br>
      <table border="1">
        <tbody>
          <?php
            echo "<tr>"; #テーブルヘッダー
              echo "<th>社員ID</th>";
              echo "<th>名前</th>";
              echo "<th>部署</th>";
              echo "<th>役職</th>";
            echo "</tr>";
            foreach ($result as $each){ #DBから持ってきた要素を各テーブルに格納
              echo "<tr>";
                echo "<td>" . $each['member_ID'] . "</td>";
                echo "<td><a href='detail.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>";
                //echo "<td>" . $each['name'] . "</td>";
                echo "<td>" . $each['section_name'] . "</td>";
                echo "<td>" . $each['grade_name'] . "</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
      </table>
<!---中身の確認
      <pre align="left">
        <?php
          var_dump($result);
        ?>
      </pre>
      --->
    </div>
  </body>
</html>
