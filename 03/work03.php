
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
  </head>

  <body>
    <form action="work03.php" method="POST">
      <table border="1" style="border-collapse: collapse">
        <!--- 表ヘッダー --->
        <div>
          <tr><!--商品名、価格、円、税抜き-->
            <th>商品名</th><th>価格（単位：円、税抜き）</th>
            <th>個数</th><th>税率</th>
          </tr>
        </div>
        <div>
          <tr>
            <td><!--商品１（５まである）-->
              <input type="text" name="goodsname1" value="" >
            </td>
            <td><!---価格１--->
              <input type="number" name="price1">
            </td>
            <td>
              <input type="number" name="goodsnum1">個
            </td>
            <td><!---税率選択--->
              <input type="radio" name="tax01" value="1.08" checked="checked">
              <label for="radio">8%</label>
              <input type="radio" name="tax01" value="1.1">
              <label for="radio">10%</label>
            </td>
          </tr>
        </div>
          <tr><!--商品２-->
            <td>
              <input type="text" name="goodsname2">
            </td>
            <td>
              <input type="number" name="price2">
            </td>
            <td>
              <input type="number" name="goodsnum2">個
            </td>
            <td>
              <input type="radio" name="tax02" value="1.08" checked="checked">
              <label for="radio">8%</label>
              <input type="radio" name="tax02" value="1.1">
              <label for="radio">10%</label>
            </td>
          </tr>
          <tr><!--商品３-->
            <td>
              <input type="text" name="goodsname3">
            </td>
            <td>
              <input type="number" name="price3">
            </td>
            <td>
              <input type="number" name="goodsnum3">個
            </td>
            <td>
              <input type="radio" name="tax03" value="8%" checked="checked">
              <label for="radio">8%</label>
              <input type="radio" name="tax10" value="10%">
              <label for="radio">10%</label>
            </td>
          </tr>
          <tr><!--商品４-->
            <td>
              <input type="text" name="goodsname4">
            </td>
            <td>
              <input type="number" name="price4">
            </td>
            <td>
              <input type="number" name="goodsnum4">個
            </td>
            <td>
              <input type="radio" name="tax04" value="8%" checked="checked">
              <label for="radio">8%</label>
              <input type="radio" name="tax10" value="10%">
              <label for="radio">10%</label>
            </td>
          </tr>
          <tr><!--商品５-->
            <td>
              <input type="text" name="goodsname5">
            </td>
            <td>
              <input type="number" name="price5">
            </td>
            <td>
              <input type="number" name="goodsnum5">個
            </td>
            <td>
              <input type="radio" name="tax05" value="8%" checked="checked">
              <label for="radio">8%</label>
              <input type="radio" name="tax05" value="10%">
              <label for="radio">10%</label>
            </td>
          </tr>
      </table>
      <!--- 送信、リセットボタン --->
      <input type="submit" name="Calculation" value="計算">
      <input type="reset" name="reset" value="リセット">
    </form>


    <!---送信先 --->
    <table border="1" style="border-collapse: collapse">
      <!--- 表ヘッダー --->
      <div>
        <tr><!--商品名、価格、円、税抜き-->
          <th>商品名</th><th>価格（単位：円、税抜き）</th>
          <th>個数</th><th>税率</th><th>小計</th>
        </tr>
      </div>
      <div><!---計算結果１--->
        <tr><!--商品-->
          <!--テキスト、数字、数字、チェックボックス-->
          <td><!--商品名１-->
            <?php
              $inpGoodsname1 = "";
              if (isset($_POST['goodsname1'])) {
                $inpGoodsname1 = $_POST['goodsname1'];
                echo "$inpGoodsname1";
              }
            ?>
          </td>
          <td align="right"><!--価格１-->
            <?php
              $inpPrice1 = 0; #価格の初期化
              if (isset($_POST['price1'])) {
                $inpPrice1 = $_POST['price1'];
                echo "$inpPrice1";
              }
            ?>
          </td>
          <td align="right"><!--個数１-->
            <?php
              $inpGoodsnum1 = 0; #個数の初期化
              if (isset($_POST['goodsnum1'])){
                $inpGoodsnum1 = $_POST['goodsnum1'];
                echo "$inpGoodsnum1";
              }
            ?>
            <label align="right">個</label>
          </td>
          <td><!--税率-->
            <?php
              $i = $_POST['tax01'];
              #ラジオボタンの選択を判定
              if ($i == 1.08) {
                echo "8%";
              }else {
                echo "10%";
              }
            ?>
          </td>
          <td><!---商品１小計--->
            <?php
              $subtotal1 = 0; #小計の初期化
              if (isset($_POST['tax01'])) {
              $subtotal1 = (($inpPrice1 * $inpGoodsnum1) * $_POST['tax01']);
              echo ceil($subtotal1);
            }
            ?>
          </td>
        </tr>
      </div>
      <div><!---計算結果２--->
        <tr><!--商品２-->
          <!--テキスト、数字、数字、チェックボックス-->
          <td><!--商品名２-->
            <?php
              if (isset($_POST['goodsname2'])) {
                $inpGoodsname2 = $_POST['goodsname2'];
                echo "$inpGoodsname2";
              }
            ?>
          </td>
          <td align="right"><!--価格２-->
            <?php
              $inpPrice2 = 0; #価格の初期化
              if (isset($_POST['price2'])) {
                $inpPrice2 = $_POST['price2'];
                echo "$inpPrice2";
              }
            ?>
          </td>
          <td align="right"><!--個数２-->
            <?php
              $inpGoodsnum2 = 0; #個数の初期化
              if (isset($_POST['goodsnum2'])){
                $inpGoodsnum2 = $_POST['goodsnum2'];
                echo "$inpGoodsnum2";
              }
            ?>
            <label align="right">個</label>
          </td>
          <td><!--税率-->
            <?php
              $i = $_POST['tax02'];
              #ラジオボタンの選択を判定
              if ($i == 1.08) {
                echo "8%";
              }else {
                echo "10%";
              }
            ?>
          </td>
          <td><!---商品２小計--->
            <?php
              $subtotal2 = 0; #小計の初期化
              if (isset($_POST['tax02'])) {
              $subtotal2 = (($inpPrice2 * $inpGoodsnum2) * $_POST['tax02']);
              echo ceil($subtotal2);
            }
            ?>
          </td>
        </tr>
      </div>
      <!---合計の計算--->
      <div>
        <td colspan="4">
          <label >合計</label>
        </td>
        <td>
          <?php
            $total = 0; #合計の初期化
            $total = ($subtotal1 + $subtotal2);
            echo ceil($total);
          ?>
        </td>
      </div>
    </table>

    <!---送信確認
    <div>
      <pre>
        <?php var_dump ($_POST); ?>
      </pre>
    </div>
    --->
  </body>
</html>
