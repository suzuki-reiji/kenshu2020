
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
    <link rel="stylesheet" href="style.css">
    <h1>ようこそ</h1>
  </head>

  <body>
    <form action="tableloop02.php" method="POST">
      <label>ループ回数を入力</label><br>
      <input type="number" name="num" value="0">
      <input type="submit" name="sbumit" value="実行">
    </form>

    <table border="1">
      <tbody>
        <?php
          for ($j=1; $j <= $_POST['num']; $j++) {
            echo "<tr>"; #入力値分行を生成
              for ($i=1; $i <= $_POST['num']; $i++) {
                echo "<td>"; #入力値分列を生成
                $kake = ($j * $i);
                echo $kake;
                echo "</td>";
              }
            echo "</tr>";
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
