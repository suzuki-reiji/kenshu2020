
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
    <link rel="stylesheet" href="style.css">
    <h1>ようこそ</h1>
  </head>

  <body>
    <form action="tableloop03.php" method="POST">
      <label>ループ回数を入力</label><br>
      <!---行の数を入力--->
      <label>行の入力</label>
      <input type="number" name="num_gyou" value="0"><br>
      <!---列の数を入力--->
      <label>列の入力</label>
      <input type="number" name="num_retsu" value="0">
      <!---ループの実行サブミットボタン--->
      <input type="submit" name="sbumit_gyou" value="実行">
    </form>

    <!---表の生成--->
    <table border="1">
      <tbody>
        <?php
          for ($j=1; $j <= $_POST['num_gyou']; $j++) {
            echo "<tr>"; #入力値分行を生成
              for ($i=1; $i <= $_POST['num_retsu']; $i++) {
                echo "<td>"; #入力値分列を生成
                echo "$j","-","$i"; #テーブルの表示内容
                echo "</td>";
              }
            echo "</tr>";
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
