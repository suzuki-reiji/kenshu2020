
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
    <link rel="stylesheet" href="style.css">
    <h1>ようこそ</h1>
  </head>

  <body>
    <form action="tableloop01.php" method="POST">
      <label>ループ回数を入力</label><br>
      <input type="number" name="num" value="0">
      <input type="submit" name="sbumit" value="実行">
    </form>

    <table border="1">
      <tbody>
        <tr>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
        <?php #入力した数値文行を生成する
        $s = 0;
          for ($i=0; $i < $_POST['num']; $i++) {
            echo
            "<tr>
              <td>あ</td>
              <td>か</td>
              <td>さ</td>
              <td>た</td>
            </tr>";
        }
        ?>
      </tbody>
    </table>
  </body>
</html>
