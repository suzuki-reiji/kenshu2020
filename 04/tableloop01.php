
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
    <link rel="stylesheet" href="style.css">
    <h1>ようこそ</h1>
  </head>

  <body>
    <form action="tableloop01.php" method="POST">
      <label>ループ回数を入力</label><br>
      <input type="number" name="num" value="0">
      <input type="submit" name="sbumit" value="実行">
    </form>

    <table>
      <?php
      for ($i=0; $i < $_['num']; $i++) {
        echo "結果";
      }
      ?>
    </table>
  </body>
</html>
