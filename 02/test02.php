
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Sample</title>
  </head>

  <body>
    名前は
    <pre> <!--- GETの中身確認 --->
      <?php
        var_dump($_GET);
        ?><br><br>
        <?php #受け取った名前の表示
        echo "あなたの名前は「";
        echo $_GET['name'];
        echo "」ですね";
      ?>
    </pre><br>
    <div><!--- 面積の計算 --->
      面積の計算<br>
      <!--- 縦→hei、横→wid -->
      縦<?php echo $_GET['hei']?> × 横<?php echo $_GET['wid']; ?><br>
        <?php $area = ($_GET['hei'] * $_GET['wid'])?>
      面積<?php echo "=$area";?><!---面積を代入した変数の表示 --->
    </div>
    <div><!--- チェックボックスの内容表示 --->
      <?php
      if(isset ($_GET['checkbox'])){
        $likefood = implode("、", $_GET['checkbox']);
        echo "$likefood";
        }
        else{
          echo "入力されていません";
        }
      ?>
    </div>
  </body>
</html>
