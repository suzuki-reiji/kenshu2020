
<!DOCTYPE html>
<?php

  $DB_DSN = "mysql:host=localhost; dbname=kenshu_test01; charset=utf8";
  $DB_USER = "testman";
  $DB_PW = "x0nwbFeT5PybEmHA";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT *
                FROM test";

  echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>飯処ウェブレッジ水道橋店</title>
  </head>
  <body>
    <br>
    <table border="1">
      <tbody>
        <?php
        /*---DBメモ
        dish_name = 料理名
        price = 価格
        genre = ジャンル
        note = メモ
        */
        echo "<tr>"; #テーブルヘッダー
          echo "<th>料理名</th>";
          echo "<th>価格</th>";
          echo "<th>ジャンル</th>";
          echo "<th>備考</th>";
        echo "</tr>";
        foreach ($result as $each){ #DBから持ってきた要素を各テーブルに格納
          echo "<tr>";
            echo "<td>" . $each['dish_name'] . "</td>";
            echo "<td>" . $each['price'] . "円</td>";
            echo "<td>" . $each['genre'] . "</td>";
            echo "<td>" . $each['note'] . "</td>";
          echo "</tr>";
        }
        ?>
      </tbody>
    </table>
  </body>
</html>
