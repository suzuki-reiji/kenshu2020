
<!DOCTYPE html>
<?php

  $DB_DSN = "mysql:host=localhost; dbname=kenshu_test01; charset=utf8";
  $DB_USER = "testman";
  $DB_PW = "x0nwbFeT5PybEmHA";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT FROM test WHERE 1 = 1";
    //商品のジャンル
    if (isset($_GET['search_genre']) && $_GET['search_genre'] != "") {
      $query_str .= " AND genre = '" . $_GET['search_genre'] . "'";
    }
    //商品名
    if (isset($_GET['search_name']) && $_GET['search_name'] != "") {
      $query_str .= " AND dish_name LIKE '%" . $_GET['search_name'] . "%'";
    }
    //値段上限値
    if (isset($_GET['search_number_up']) && $_GET['search_number_up'] != "") {
      $query_str .= " AND price < '" . $_GET['search_number_up'] . "'";
    }
    //値段下限値
    if (isset($_GET['search_number_bt']) && $_GET['search_number_bt'] != "") {
      $query_str .= " AND price > '" . $_GET['search_number_bt'] . "'";
    }


  echo $query_str; //中身の確認用
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>飯処ウェブレッジ水道橋店</title>
  </head>
  <!---検索フォーム--->
  <form action="sql_test03.php" method="GET">
    <p>料理の検索</p>
    <label>商品の名前</label>
      <input type="text" name="search_name" placeholder="検索したい名前(またはその一部)を入力" size="40"><br>
    <label>商品の値段</label><br>上限
      <input type="number" name="search_number_up" value="0">~
      <input type="number" name="search_number_bt" value="0">まで
    <select name="search_genre">
      <option value="0">種類を選択</option>
      <option value="麺類">麺類</option>
      <option value="米類">米類</option>
      <option value="汁類">汁類</option>
      <option value="肉類">肉類</option>
      <option value="福副菜">福副菜</option>
    </select>
    <br>
    <input type="submit" name="search" value="検索">
  </form>


  <body>
    <br>
    <table border="1">
      <tbody>
        <?php
        /*---DBメモ
        dish_name = 料理名
        price = 価格
        genre = ジャンル
        note = メモ
        */
        echo "<tr>"; #テーブルヘッダー
          echo "<th>料理名</th>";
          echo "<th>価格</th>";
          echo "<th>ジャンル</th>";
          echo "<th>備考</th>";
        echo "</tr>";
        foreach ($result as $each){ #DBから持ってきた要素を各テーブルに格納
          echo "<tr>";
            echo "<td>" . $each['dish_name'] . "</td>";
            echo "<td>" . $each['price'] . "円</td>";
            echo "<td>" . $each['genre'] . "</td>";
            echo "<td>" . $each['note'] . "</td>";
          echo "</tr>";
        }
        ?>
      </tbody>
    </table>
  </body>
</html>
